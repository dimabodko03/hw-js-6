function createNewUser() {
    const newUser = {
        firstName: prompt('Write ur name:'),
        lastName: prompt('Write ur last name:'),
        birthday: prompt('Write ur birthday (dd.mm.yyyy):'),

        getLogin: function() {
            return `${this.firstName[0]}`.toLowerCase() + `${this.lastName}`.toLowerCase();
        },

        getAge: function() {
            const birthArray = this.birthday.split('.');
            const today = new Date();
            const birthDate = new Date(birthArray[2], birthArray[1] - 1, birthArray[0]);
            const age = today.getFullYear() - birthDate.getFullYear();
            const month = today.getMonth() - birthDate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },

        getPassword: function() {
            const birthYear = this.birthday.split('.')[2];
            return `${this.firstName[0]}`.toUpperCase() + `${this.lastName}`.toLowerCase() + `${birthYear}`;
        }
    };

    return newUser;
}

const user = createNewUser();
console.log(`Login: ${user.getLogin()}`);
console.log(`Age: ${user.getAge()}`);
console.log(`Password: ${user.getPassword()}`);
